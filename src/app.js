import React from 'react'; // Импорт React
import ReactDOM from 'react-dom'; // Импорт ReactDOM (здесь происходит рендеринг)
import AppRouter from './routers/AppRouter';
import 'normalize.css/normalize.css';
import './styles/main.sass'; // 

ReactDOM.render(<AppRouter />, document.getElementById('app')); // Рендеринг на страницу