import React from 'react'; // Импорт React
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom';
import MainPage from '../components/MainPage';
import OnlineCasinoPage from '../components/OnlineCasinoPage';
import SlotsPage from '../components/SlotsPage';
import BonusPage from '../components/BonusPage';
import Blog from '../components/Blog';
import NotFoundPage from '../components/NotFoundPage';
import Header from '../components/Header';
import Footer from '../components/Footer';

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Header />
        <Switch>
          <Route path="/" component={MainPage} exact={true} />
          <Route path="/online-casino" component={OnlineCasinoPage} exact={true} />
          <Route path="/slots" component={SlotsPage} exact={true} />
          <Route path="/bonus" component={BonusPage} />
          <Route path="/blog" component={Blog} />
          <Route component={NotFoundPage} />
        </Switch>
      <Footer />
    </div>
  </BrowserRouter>
);

export default AppRouter;