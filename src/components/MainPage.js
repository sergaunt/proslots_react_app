import React from 'react'
import SlotsDashboard from './SlotsDashboard'
import TextItem from './TextItem'
import JackpotListTable from './JackpotListTable'
import CasinoRankingTable from './CasinoRankingTable'
import BonusesListTable from './BonusesListTable'
import ArticlesListTable from './ArticlesListTable'

export default () => {
  const text1 = 'Привет ipsum dolor sit amet, consectetur adipisicing elit. Est a minus, adipisci asperiores totam? Ab optio, cum impedit cupiditate numquam, unde! Expedita nisi, dolorem minus qypuajgyui accusamus unde delectus consectetur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci dolorem modi sunt, quas debitis consequuntur commodi error possimus, quia, rem culpa porro aspernatur. Nesciunt quam nostrum tempore quia, porro impedit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident magnam fugiat recusandae unde, molestiae nesciunt aspernatur cupiditate rem modi at sint blanditiis, repudiandae temporibus sed perferendis, commodi quibusdam veritatis quos.'
  
  return (
    <main className='grid-3-to-1-columns'>
      <section className='main-content'>
        <SlotsDashboard />
        <TextItem content={text1}/>
        <div className='grid-1-to-1-columns grid-2-to-1-columns'>
          <JackpotListTable />
          <CasinoRankingTable />
        </div>
      </section>
      <section className='sidebar-content grid-1-to-1-columns'>
        <BonusesListTable />
        <ArticlesListTable />
      </section>
    </main>
  )
}