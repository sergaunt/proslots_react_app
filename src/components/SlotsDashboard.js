import React from 'react'
import SlotMenuItem from './SlotMenuItem'
import ButtonMore from './ButtonMore'

export default () => {
  const slots = ['slot1', 'slot2', 'slot3', 'slot4', 'slot5', 'slot6', 'slot7', 'slot8', 'slot9', 'slot10', 'slot11', 'slot12']
  
  return (
    <div>
      <h1>Лучшие бесплатные игры</h1>
      <div className='container-2-to-4'>
        { slots.map((slot) => (
            <SlotMenuItem key={slot} name={slot} />
          ))}
      </div>
      <ButtonMore text = 'Больше игр' link = 'slots' specialClass = 'purple-button' />
    </div>
  )
}