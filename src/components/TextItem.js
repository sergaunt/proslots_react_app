import React from 'react'

export default (props) => (
  <div className='text-field'>
    <p>{props.content}</p>
  </div>
)