import React from 'react'
import ButtonMore from './ButtonMore'
import JackpotListItem from './JackpotListItem'
import uuid from 'uuid'

export default () => {
  const jackpots = [
    {
      name: 'Grand Theft Auto Vice City',
      amount: '999,999,999',
      platform: 'Netent'
    },
    {
      name: 'Need For Speed Most Wanted',
      amount: '1',
      platform: 'MicroGaming'
    },
    {
      name: 'Grand Theft Auto Vice City',
      amount: '999,999,999',
      platform: 'Netent'
    },
    {
      name: 'Need For Speed Most Wanted',
      amount: '1',
      platform: 'MicroGaming'
    },
    {
      name: 'Grand Theft Auto Vice City',
      amount: '999,999,999',
      platform: 'Netent'
    }
  ]

  return (
    <div className='jackpots-wrapper'>
      <h2>Джекпоты</h2>
      <table className='table jackpot-table container-style-main'>
        <tbody>
          <tr className='table-purple-menu'>
            <th>Слот</th>
            <th className='table-cell-purple'>Джекпот</th>
            <th>Платформа</th>
          </tr>
          {jackpots.map((jackpot) => <JackpotListItem key={uuid()} jackpot={jackpot} />)}
          <tr data-href="#" className="table-button-row clickable">
            <td colSpan="3">
              <ButtonMore text='Больше джекпотов' link='jackpots' />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}