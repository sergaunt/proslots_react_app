import React from 'react'

export default (props) => {
  return (
    <tr data-href="#" className="clickable">
      <td className="table-picture picture-wide">
        <picture>
          <source media="(max-width: 1023px)" srcSet={require('../img/bonus/1/mob.jpg')} />
          <img src={require('../img/bonus/1/wide.jpg')} alt="iPhone" />
        </picture>
        {props.bonus}
      </td>
    </tr>
  )
}