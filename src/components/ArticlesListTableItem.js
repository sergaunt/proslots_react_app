import React from 'react'

export default (props) => {
  return (
    <tr>
      <td>
        <p className="article-item">
          <span className="article-name">{props.article.title}</span>
          <span className="article-annotation">{props.article.description}</span>
        </p>
      </td>
    </tr>
  )
}