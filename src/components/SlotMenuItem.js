import React from 'react'

export default (props) => {
  return (
    <div className='item-style-main item-2-to-4 slots-item'>
      <picture className='picture-item-2-to-4 picture-slots-item'>
        <source media='(max-width: 320px)' srcSet={require('../img/slots/1/mob.jpg')} />
        <source media='(max-width: 1023px)' srcSet={require('../img/slots/1/tablet.jpg')} />
        <img src={require('../img/slots/1/wide.jpg')} alt={props.name} />
      </picture>
      <div className='name-item-2-to-4 slot-name'>{props.name}</div>
      <a className='slots-item-link' href={props.name}>
        <span className='button-more'>
          Играть
        </span>
      </a>
    </div>
  )
}