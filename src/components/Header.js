import React from 'react'
import { NavLink } from 'react-router-dom'

export default () => (
  <header>
		<section className="l-header-container">
      <NavLink to="/" className="logo" exact={true}></NavLink>
			<nav className="header-menu">
				<button className="menu-button">
          <div className="hamburger"></div>
        </button>
				<ul className="menu-main">
					<li>
            <NavLink to="/online-casino" activeClassName="header-is-active" exact={true}>Онлайн-казино</NavLink>
						<ul className="menu-second">
							<li><NavLink to="/online-casino/roulette" exact={true}>Рулетка</NavLink></li>
							<li><NavLink to="/online-casino/baccara" exact={true}>Баккара</NavLink></li>
							<li><NavLink to="/online-casino/blackjack" exact={true}>Blackjack</NavLink></li>
							<li><NavLink to="/online-casino/other" exact={true}>Другие игры</NavLink></li>
						</ul>
					</li>
					<li>
            <NavLink to="/slots" activeClassName="header-is-active" exact={true}>Игровые автоматы</NavLink>
						<ul className="menu-second">
							<li><NavLink to="/slots/popular">Самые популярные слоты</NavLink></li>
							<li><NavLink to="/slots/one-hand">Однорукие бандиты</NavLink></li>
							<li><NavLink to="/slots/3d">3D Слоты</NavLink></li>
							<li><NavLink to="/slots/progressive">Прогрессивные слоты</NavLink></li>
							<li><NavLink to="/slots/ra">RA</NavLink></li>
						</ul>
					</li>
					<li>
            <NavLink to="/bonus" activeClassName="header-is-active" exact={true}>Бонусы</NavLink>
          </li>
					<li>
            <NavLink to="/blog" activeClassName="header-is-active" exact={true}>Статьи</NavLink>
          </li>
				</ul>
			</nav>
		</section>    
  </header>
)
