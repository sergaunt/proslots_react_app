import React from 'react'

export default (props) => {
  return (
    <tr data-href='#' className='clickable'>
      <td className='table-picture'>
        <picture>
          <source media='(max-width: 1023px)' srcSet={require('../img/jackpots/1/mob.jpg')} />
          <img src={require('../img/jackpots/1/wide.jpg')} alt='iPhone' />
        </picture>
        {props.jackpot.name}
      </td>
      <td className='table-cell-white'>${props.jackpot.amount}</td>
      <td>{props.jackpot.platform}</td>
    </tr>
  )
}
