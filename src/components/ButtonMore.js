import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class ButtonMore extends Component {
  constructor(props) {
    super(props)

    const buttonClass = window.innerWidth < 1024 ? 'button-more' : this.props.specialClass

    this.state = {
      buttonClass
    }
  }

  componentDidMount() {
    window.addEventListener('resize', () => {
      this.setState({
        buttonClass: window.innerWidth < 1024 ? 'button-more' : this.props.specialClass
      })      
    })
  }

  render() {
    const { text, link } = this.props
    return (
      <div>
        <Link className={this.state.buttonClass} to={link}>{text}</Link>
      </div>
    )
  }
}

export default ButtonMore