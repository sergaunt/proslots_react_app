import React from 'react'
import social from '../img/social.svg'

export default () => (
  <footer>
    <section className='l-footer-container'>
      <img className="footer-slot-machine" src={require('../img/slot-machine.svg')} />
      <p>
        Все игровые автоматы бесплатно представлены на сайте сугубо в ознакомительных целях.<br />Копирование материалов сайта без обратной ссылки запрещено.
      </p>
      <nav className="footer-menu">
        <ul>
          <li>
            <a href="online-casino">Онлайн-казино</a>
          </li>
          <li>
            <a href="slots">Игровые автоматы</a>
          </li>
          <li>
            <a href="bonus">Бонусы</a>
          </li>
          <li>
            <a href="blog">Статьи</a>
          </li>
        </ul>
      </nav>
      <p>Мы в соцсетях:</p>
      <div className="social">
        <a href="#">
          <svg><use xlinkHref={social+'#vk-logo'} /></svg>
        </a>
        <a href="#">
          <svg><use xlinkHref={social+'#fb-logo'} /></svg>
        </a>
        <a href="#">
          <svg><use xlinkHref={social+'#in-logo'} /></svg>
        </a>
      </div>
      <p>
        Сopyright © 2018- proslots.ru <br />proslots@proslots.ru
      </p>
    </section>
  </footer>
)