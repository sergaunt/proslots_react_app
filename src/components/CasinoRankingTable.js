import React from 'react'
import ButtonMore from './ButtonMore'
import CasinoRankingTableItem from './CasinoRankingTableItem'
import uuid from 'uuid'

export default () => {
  const casinos = [
    {
      name: 'Red Pinguin Casino',
      rate: 5,
    },
    {
      name: 'Blue Pinguin Casino',
      rate: 3.8,
    },
    {
      name: 'Green Pinguin Casino',
      rate: 0.3,
    },
    {
      name: 'Yellow Pinguin Casino',
      rate: 4.6,
    },
    {
      name: 'Red Pinguin Casino',
      rate: 5,
    },
  ]

  return (
    <div className='ranking-wrapper'>
      <h2>Рейтинг Казино</h2>
      <table className='table ranking-table container-style-main'>
        <tbody>
          <tr className='table-purple-menu'>
            <th colSpan="2">Лучшие казино</th>
          </tr>
          {casinos.map((casino) => <CasinoRankingTableItem key={uuid()} casino={casino} />)}
          <tr data-href="#" className="table-button-row clickable">
            <td colSpan="4">
              <ButtonMore text='Полный рейтинг' link='ranking' />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}
