import React from 'react'
import ButtonMore from './ButtonMore'
import ArticlesListTableItem from './ArticlesListTableItem'
import uuid from 'uuid'

export default () => {
  const articles = [
    {
      title: 'ХОЧУ ДЕНЬГИ В КАЗИНО, ЧТО ДЕЛАТЬ?',
      description: 'В данной статье мы расскажем вам о том, как получить деньги в онлайн казино и начать играть на реальные деньги в азартные игры.'
    },
    {
      title: 'ХОЧУ ДЕНЬГИ В КАЗИНО, ЧТО ДЕЛАТЬ?',
      description: 'В данной статье мы расскажем вам о том, как получить деньги в онлайн казино и начать играть на реальные деньги в азартные игры.'
    },
    {
      title: 'ХОЧУ ДЕНЬГИ В КАЗИНО, ЧТО ДЕЛАТЬ?',
      description: 'В данной статье мы расскажем вам о том, как получить деньги в онлайн казино и начать играть на реальные деньги в азартные игры.'
    },
    {
      title: 'ХОЧУ ДЕНЬГИ В КАЗИНО, ЧТО ДЕЛАТЬ?',
      description: 'В данной статье мы расскажем вам о том, как получить деньги в онлайн казино и начать играть на реальные деньги в азартные игры.'
    },
    {
      title: 'ХОЧУ ДЕНЬГИ В КАЗИНО, ЧТО ДЕЛАТЬ?',
      description: 'В данной статье мы расскажем вам о том, как получить деньги в онлайн казино и начать играть на реальные деньги в азартные игры.'
    }
  ]

  return (
    <div className='articles-wrapper'>
      <h2>Статьи</h2>
      <table className="table articles-table container-style-main">
        <tbody>
          <tr></tr>
          {articles.map((article) => <ArticlesListTableItem key={uuid()} article={article} />)}
          <tr data-href="#" className="table-button-row clickable">
            <td>
              <ButtonMore text='Все статьи' link='blog' />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}