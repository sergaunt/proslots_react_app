import React from 'react'

export default (props) => {
  let classOfCasino = 'table-cell-white ranking-value rank-'
  if (props.casino.rate < 3) {
    classOfCasino += 'red';
  } else if (props.casino.rate < 4) {
    classOfCasino += 'orange';
  } else if (props.casino.rate < 5) {
    classOfCasino += 'yellow';
  } else {
    classOfCasino += 'green';
  }

  return (
    <tr data-href="#" className="clickable">
      <td className="table-picture picture-wide">
        <picture>
          <source media="(max-width: 1023px)" srcSet={require('../img/ranking/1/mob.jpg')} />
          <img src={require("../img/ranking/1/wide.jpg")} alt="iPhone" />
        </picture>
        {props.casino.name}
      </td>
      <td className={classOfCasino}>
        {props.casino.rate}
      </td>
    </tr>
  )
}
