import React from 'react'
import BonusesListTableItem from './BonusesListTableItem'
import ButtonMore from './ButtonMore'
import uuid from 'uuid'

export default () => {
  const bonuses = [
    'Приветственный бонус до $100 при регистрации', 
    '50 ФРИСПИНОВ для Jurassic Park',
    'БЕЗДЕПОЗИТНЫЙ БОНУС 25$ от Вулкан Оригинал',
    'БЕЗДЕПОЗИТНЫЙ БОНУС 15$ от Олимп',
    'Приветственный бонус до $100 при регистрации'
  ]

  return (
    <div className='bonuses-wrapper'>
      <h2>Бонусы</h2>
      <table className="table container-style-main">
        <tbody>
          <tr className="table-purple-menu">
            <th>Лучшие бонусы</th>
          </tr>
          {bonuses.map((bonus) => <BonusesListTableItem key={uuid()} bonus={bonus} />)}
          <tr data-href="bonus" className="table-button-row clickable">
            <td colSpan="4">
              <ButtonMore text='Больше бонусов' link='bonus' />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}
