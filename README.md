# Udemy.com React Course

- Installing yarn:

npm install -g yarn

- Installing live-server

yarn global add live-server
npm install -g live-server

- Running live-server

live-server public

- Installing babel-cli

yarn global add babel-cli
npm install -g babel-cli

- Yarn initialization and packages installing

yarn init
yarn install

- Adding babel presets

yarn add babel-preset-react babel-preset-env

- Starting compiling

babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch

- Remove global dependencies

yarn global remove babel-cli live-server
npm unistall -g babel-cli live-server

- Installing local dependencies
yarn add live-server babel-cli webpack babel-core babel-loader webpack-dev-server react-modal

- Installing React
yarn add react react-dom

- Installing babel plugin
yarn add babel-plugin-transform-class-properties
yarn add style-loader css-loader sass-loader node-sass
yarn add normalize.css